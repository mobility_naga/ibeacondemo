//
//  ViewController.m
//  iBeaconDemo
//
//  Created by wei.xiang on 5/26/14.
//  Copyright (c) 2014 wei.xiang. All rights reserved.
//

#import "ViewController.h"
#import "ESTBeaconManager.h"
#import "ESTBeacon.h"

@interface ViewController ()<ESTBeaconManagerDelegate>

@property (nonatomic,retain) UIScrollView *scroll;
@property (nonatomic,retain) UIImageView *iv;
@property (nonatomic,retain) UIView *subview;

@property (nonatomic, strong) ESTBeacon         *beacon;
@property (nonatomic, strong) ESTBeaconManager  *beaconManager;
//@property (nonatomic, strong) ESTBeaconRegion   *beaconRegion;
@property (nonatomic, strong) ESTBeaconRegion *region;
@property (nonatomic, strong) NSArray *beaconsArray;

@property (nonatomic,retain) UIProgressView *progress;
@property (nonatomic,weak) NSTimer *timer;
@property (nonatomic,strong) UIActionSheet *actionSheet;

@end

@implementation ViewController
{
    CLLocationManager *_locationManager;
    
    CLBeaconRegion *beaconRegion;
}

//#define ESTIMOTE_PROXIMITY_UUID             [[NSUUID alloc] initWithUUIDString:@"B9407F30-F5F8-466E-AFF9-25556B57FE6D"]

- (id)initWithBeacon:(ESTBeacon *)beacon
{
    self = [super init];
    if (self)
    {
        self.beacon = beacon;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.beaconManager = [[ESTBeaconManager alloc] init];
    self.beaconManager.delegate = self;
    
    self.region = [[ESTBeaconRegion alloc] initWithProximityUUID:ESTIMOTE_PROXIMITY_UUID
                                                      identifier:@"EstimoteSampleRegion"];
    
    [self initScrollView];

    [self.beaconManager startRangingBeaconsInRegion:self.region];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self.beaconManager stopRangingBeaconsInRegion:self.region];
    
    [super viewDidDisappear:animated];
}

bool isFind =false;
#pragma mark - ESTBeaconManager delegate

- (void)beaconManager:(ESTBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region
{
    self.beaconsArray = beacons;
    
    if (isFind) {
        return;
    }
    
    if (beacons.count > 0)
    {
        isFind = YES;
        ESTBeacon *firstBeacon = [beacons firstObject];
        
        NSString *msg =[NSString stringWithFormat:@"%@;%@%@",[NSString stringWithFormat:@"%@",firstBeacon.proximityUUID],[NSString stringWithFormat:@"Major: %@, Minor: %@", firstBeacon.major, firstBeacon.minor],
                        [NSString stringWithFormat:@"Distance: %.2f", [firstBeacon.distance floatValue]]];
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Information"
                              message:@"find a ibeacon location,will show your posiztion..."
                              delegate:self
                              cancelButtonTitle:@"sure"
                              otherButtonTitles:nil];
        alert.tag = 0;
        [alert show];
        
        
        switch (firstBeacon.proximity) {
            case CLProximityFar:
                NSLog(@"Far");
                break;
            case CLProximityNear:
                NSLog(@"Near");
                break;
            case CLProximityImmediate:
                NSLog(@"Immediate");
                break;
                
            default:
                NSLog(@"Unknown");
                break;
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"buttonIndex:%d",buttonIndex);
    if (alertView.tag==1 && buttonIndex==0) {
        _actionSheet = [[UIActionSheet alloc] initWithTitle:@"Downloading file,please wait for a moment!\n\n\n" delegate:nil cancelButtonTitle:nil destructiveButtonTitle: nil otherButtonTitles: nil];
        
        _progress = [[UIProgressView alloc] initWithFrame:CGRectMake(50.0f, 40.0f, 480.0f, 90.0f)];
        
        [_progress setProgressViewStyle:UIProgressViewStyleDefault]; //设置进度条类型
        [_progress setProgress:0.0f];
        [_actionSheet addSubview:_progress];
        
        UIView *curView = [[UIView alloc] init];
        curView.frame = CGRectMake(200, 200, 524, 320);
        [_iv addSubview:curView];
        
        [_actionSheet showInView:_scroll];
        
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(changeProgress) userInfo:nil repeats:YES];
        
    }
    else
    {
        [self createMainButton];
    }
}

CGFloat value;
-(void)changeProgress
{
    NSLog(@"%f",value);
    if(value<1)
    {
        value = value+0.1;
        [_progress setProgress:value];
    }
    else
    {
        value=0;
        [_progress setProgress:0];
        [_timer invalidate];
        [_actionSheet dismissWithClickedButtonIndex:0 animated:YES];
        _actionSheet = nil;
    }
}

-(void)initScrollView
{
    _scroll = [[UIScrollView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    _scroll.backgroundColor = [UIColor blackColor];
    _scroll.delegate = self;
    _iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"analyst4-iPad.png"]];
    
    [_scroll addSubview:[self createScrollBackView]];
    _scroll.minimumZoomScale = 0.5;
    _scroll.maximumZoomScale = 1;
    [_scroll setZoomScale:_scroll.minimumZoomScale];
    self.view = _scroll;
}

-(UIView *)createScrollBackView
{
    _subview = [[UIView alloc] init];
    _subview.frame =CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.height*2, [[UIScreen mainScreen] applicationFrame].size.width*2+40);
    
    _iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"analyst4-iPad.png"]];
    
    _iv.frame = CGRectMake(0, 0, _subview.frame.size.width, _subview.frame.size.height);
    
    [_subview addSubview:_iv];
    
    UILabel *lb = [[UILabel alloc] init];
    lb.text = @"Click flash picture";
    lb.frame = CGRectMake(605, 255, 150, 32);
    [_subview addSubview:lb];
    
    return _subview;
}

-(void)createMainButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.frame = CGRectMake(565, 280, 35, 35);
    btn.alpha = 1;
    [btn setBackgroundImage:[UIImage imageNamed:@"position-iPad.png"] forState:UIControlStateNormal];
    [_subview addSubview:btn];
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn2.frame = CGRectMake(565, 280, 35, 35);
    btn2.alpha = 0.1;
    [btn2 setBackgroundImage:[UIImage imageNamed:@"position-iPad.png"] forState:UIControlStateNormal];
    [_subview addSubview:btn2];
    [btn2 addTarget:self action:@selector(zoomInAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [UIView animateWithDuration:1.5 delay:0 options:UIViewAnimationOptionRepeat animations:^{
        if (btn.alpha == 1) {
            [btn setAlpha:0.2];
        }else if (btn.alpha == 0.2){
            [btn setAlpha:1];
        }
    } completion:^(BOOL finished) {
        
    }];
}

-(void)initLocationManager
{
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"E2C56DB5-DFFB-48D2-B060-D0F5A71096E0"];
    beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
                                                      identifier:@"com.accenture.BeaconDemo"];
    
    // This location manager for ranging beacons.
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
}

-(void)zoomInAction:(id)sender
{
    [UIView animateWithDuration:1 animations:^{
        [_scroll setZoomScale:_scroll.maximumZoomScale];
        _iv.alpha = 0.7;
    } completion:^(BOOL finished){
        _iv.alpha = 0;
        
        [UIView animateWithDuration:1 animations:^{
            _iv.image = [UIImage imageNamed:@"roomPic2.jpg"];
            
            _iv.alpha = 1;
        }completion:^(BOOL finished){
            [UIView animateWithDuration:1 animations:^{
                [_scroll setZoomScale:_scroll.minimumZoomScale];
            }];
        }];
    }];
    
    [self createLocationButton];
}

-(void)createLocationButton
{
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn2.frame = CGRectMake(565, 280, 35, 35);
    btn2.alpha = 0.1;
    [btn2 setBackgroundImage:[UIImage imageNamed:@"position-iPad.png"] forState:UIControlStateNormal];
    [_subview addSubview:btn2];
    [btn2 addTarget:self action:@selector(downloadData) forControlEvents:UIControlEventTouchUpInside];
}

-(void)downloadData
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Do you want to download the artist's detail informations where you are" delegate:self cancelButtonTitle:@"Sure"
                          otherButtonTitles:@"Cancel",nil];
    alert.tag = 1;
    [alert show];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
//    CGFloat offsetX = (_scroll.bounds.size.width > _scroll.contentSize.width)?
//    (_scroll.bounds.size.width - _scroll.contentSize.width) * 0.5 : 0.0;
//    CGFloat offsetY = (_scroll.bounds.size.height > _scroll.contentSize.height)?
//    (_scroll.bounds.size.height - _scroll.contentSize.height) * 0.5 : 0.0;
//    _iv.center = CGPointMake(_scroll.contentSize.width * 0.5 + offsetX,
//                            _scroll.contentSize.height * 0.5 + offsetY);
    
//    _scroll.contentOffset = CGPointMake(450, 300);
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return _subview;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Start browsing for beacons
- (void)startRangingBeacons
{
    [_locationManager startRangingBeaconsInRegion:beaconRegion];
}

- (void)stopRangingBeacons
{
    [_locationManager stopRangingBeaconsInRegion:beaconRegion];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self startRangingBeacons];
}

@end
