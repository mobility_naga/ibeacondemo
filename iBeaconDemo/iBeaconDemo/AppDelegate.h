//
//  AppDelegate.h
//  iBeaconDemo
//
//  Created by wei.xiang on 5/26/14.
//  Copyright (c) 2014 wei.xiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
