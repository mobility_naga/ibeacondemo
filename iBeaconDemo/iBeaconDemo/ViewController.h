//
//  ViewController.h
//  iBeaconDemo
//
//  Created by wei.xiang on 5/26/14.
//  Copyright (c) 2014 wei.xiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>


@interface ViewController : UIViewController<CLLocationManagerDelegate,UIScrollViewDelegate>

@end
